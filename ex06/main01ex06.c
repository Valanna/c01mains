#include <stdio.h>
int	ft_strlen(char *str);

int	main(void)
{
	char	*str;
	char	*str2;

	str = "test";
	printf("%i: %s\n", ft_strlen(str), str);
	str2 = "Create a function that counts and returns the number of characters in a string.";
	printf("%i: %s\n", ft_strlen(str2), str2);
	return(0);
}