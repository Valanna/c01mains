#include <stdio.h>
void	ft_ultimate_div_mod(int *a, int *b);

int	main(void)
{
	int	i;
	int	j;

	i = 42;
	j = 4;
	printf("%i and %i\n", i, j);
	ft_ultimate_div_mod(&i, &j);
	printf("%i left %i\n", i, j);
}