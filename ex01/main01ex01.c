#include <stdio.h>
void	ft_ultimate_ft(int *********nbr);

int	main(void)
{
	int i = 13;
	int *nbra = &i;
	int **nbrb = &nbra;
	int ***nbrc = &nbrb;
	int ****nbrd = &nbrc;
	int *****nbre = &nbrd;
	int ******nbrf = &nbre;
	int *******nbrg = &nbrf;
	int ********nbrh = &nbrg;

	printf("%i\n", i);
	ft_ultimate_ft(&nbrh);
	printf("%i\n", i);
	return(0);
}