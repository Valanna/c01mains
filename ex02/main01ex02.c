#include <stdio.h>
void	ft_swap(int *a, int *b);

int	main(void)
{
	int i;
	int j;

	i = 13;
	j = 42;
	printf("%i and %i\n", i, j);
	ft_swap(&i, &j);
	printf("%i and %i\n", i, j);
}