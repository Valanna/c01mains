#include <stdio.h>
void	ft_sort_int_tab(int *tab, int size);

int	main(void)
{
	int	array[]= {42, 13, 123, 24, 0, 1, 2, 3 ,4, 5, 6, 7, 8, 9, 2147483647, -999};
	int i;
	int	size = sizeof(array) / sizeof(int);

	i = 0;
	while (i < size)
	{
		printf("%d ",array[i]);
		i++;
	}
	printf("| ");
	ft_sort_int_tab(array, size);
	i = 0;
	while (i < size)
	{
		printf("%d ",array[i]);
		i++;
	}
	return (0);
}