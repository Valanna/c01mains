#include <stdio.h>

void	ft_div_mod(int a, int b, int *div, int *mod);

int	main(void)
{
	int	i;
	int	j;
	int res;
	int mod;

	i = 42;
	j = 4;
	res = 0;
	mod = 0;
	printf("%i and %i\n", i, j);
	ft_div_mod(i, j, &res, &mod);
	printf("%i/%i = %i left %i\n", i, j, res, mod);
}